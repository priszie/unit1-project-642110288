using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pitchaya.GameDev3.Chapter1
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
    }
}